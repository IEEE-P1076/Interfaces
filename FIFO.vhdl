-- Package author:    Patrick Lehmann - Patrick.Lehmann@tu-dresden.de
-- =============================================================================
-- Interface name:    PoC.FIFO
-- Developed by:      VLSI-EDA
-- Standard/Manual:   http://poc-library.readthedocs.io/en/latest/References/Interfaces/index.html
-- =============================================================================
-- 
package PoC_FIFO is
	type B_FIFO_WritePort_Simple is record
		Put  : std_logic;
		Data : std_logic_vector;
		Full : std_logic;
	end record;

	view V_FIFO_WritePort_Simple of B_FIFO_WritePort_Simple is
		Put  : in;
		Data : in;
		Full : out;
	end view;
	
	type B_FIFO_WritePort is record
		Put        : std_logic;
		Data       : std_logic_vector;
		Full       : std_logic;
		EmptyState : std_logic_vector;
	end record;

	view V_FIFO_WritePort of B_FIFO_WritePort is
		Put        : in;
		Data       : in;
		Full       : out;
		EmptyState : out;
	end view;
	
	type B_FIFO_ReadPort_Simple is record
		Got       : std_logic;
		Data      : std_logic_vector;
		Valid     : std_logic;
	end record;
	
	view V_FIFO_ReadPort_Simple of B_FIFO_ReadPort_Simple is
		Got   : in;
		Data  : out;
		Valid : out;
	end view;
	
	type B_FIFO_ReadPort is record
		Got       : std_logic;
		Data      : std_logic_vector;
		Valid     : std_logic;
		FillState : std_logic_vector;
	end record;
	
	view V_FIFO_ReadPort of B_FIFO_ReadPort is
		Got       : in;
		Data      : out;
		Valid     : out;
		FillState : out;
	end view;
	
	-- -- common clock FIFO
	-- component fifo_cc_got is
		-- generic (
			-- DATA_BITS        : positive;
			-- EMPTY_STATE_BITS : natural;
			-- FILL_STATE_BITS  : natural
		-- );
		-- port (
			-- Clock  : in std_logic;
			-- Reset  : in std_logic;
			
			-- view WritePort : view V_FIFO_WritePort; -- B_FIFO_WritePort;
			-- view ReadPort  : view V_FIFO_ReadPort   -- B_FIFO_ReadPort
		-- );
	-- end component;
	
	-- -- independent clock / cross-clock FIFO
	-- component fifo_ic_got is
		-- generic (
			-- DATA_BITS        : positive;
			-- EMPTY_STATE_BITS : natural;
			-- FILL_STATE_BITS  : natural
		-- );
		-- port (
			-- WritePort_Clock : in std_logic;
			-- WritePort_Reset : in std_logic;
			-- view WritePort_Data  : view V_FIFO_WritePort; -- B_FIFO_WritePort;
			
			-- ReadPort_Clock  : in std_logic;
			-- ReadPort_Reset  : in std_logic;
			-- view ReadPort_Data   : view V_FIFO_ReadPort    --  B_FIFO_ReadPort
		-- );
	-- end component;
end package;

-- General Comments:
-- =====================================
-- * <Add your comment here>

