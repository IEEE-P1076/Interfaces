# Collected Interfaces

## Links:
IEEE P1076 Working Group: http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/WebHome  
Interfaces and view Enhancements: http://www.eda-twiki.org/cgi-bin/view.cgi/P1076/InterfaceAndviewEnhancements  

## Git in a GUI

**Using Git in a GUI (recommended):**  
 * **SourceTree** for Windows and Max OS  
   **=>** https://www.sourcetreeapp.com/  
 * **SmartGit** for Linux or Windows  
   **=>** http://www.syntevo.com/smartgit/  

**Using Git in PowerShell:**
 * **PoshGit** a PowerShell addon  
   **=>** http://dahlbyk.github.io/posh-git/

Please clone this repository onto your local machine and rename the default remote called `origin` to `gitlab`. This gives nicer messages in automatic Git messages.
```Bash
git remote rename origin gitlab
```

## Git on Console

#### PowerShell

Add the command `git tree` and `git treea` to Git. These commands display the commit tree as colored ASCII art
in the console window.

```PowerShell
git config --global alias.tree 'log --decorate --pretty=oneline --abbrev-commit --date-order --graph'
git config --global alias.treea 'log --decorate --pretty=oneline --abbrev-commit --date-order --graph --all'
```

#### Bash

Add the command `git tree` and `git treea` to Git. These commands display the commit tree as colored ASCII art
in the console window. Some Linux distributions need to enable colors in the pager (e.g. `less`).

```Bash
git config --global alias.tree 'log --decorate --pretty=oneline --abbrev-commit --date-order --graph'
git config --global alias.treea 'log --decorate --pretty=oneline --abbrev-commit --date-order --graph --all'
```


## Writing Interfaces 

 * Please put each interface into at least one VHDL package.
 * Please put your example entities like masters and slaves, if not needed as a utility entity, into a separate file

## List of Interfaces

 * Tri-State
 * LVDS (incl. lanes)
 * I²C
 * JTAG
 * VGA
 * PoC.CSE (Command-Status-Error)
 * PoC.FIFO (read and write ports of a FIFO)
 * PicoBlaze
 * LCDBus
 * PCI Express
 * AMBA AXI4 (Lite)
 * AMBA AHB (Lite)
 * \<Add your interface name here\>

## Syntax Example

```vhdl
-- Step 1 - Create a record type with members
type foo is record
  member1 : std_logic;
  member2 : std_logic;
end record;

-- Step 2 - Create a view (view) with directions
view foo_b of foo is
  member1 : in;
  member2 : out;
end view;

-- Step 3 - Records can be composed
type foo_a is array(0 to 7) of foo;

-- Step 4 - Nesting records
type bar is record
  sub_rec : foo;
  member3 : std_logic;
  member4 : std_logic;
end record;

-- Step 5 - Nesting views
view bar_b of bar is
  view sub_rec : foo_b;  -- refer to the view defined for foo
  member3 : in;
  member4 : out;
end view;

-- Step 6 - Use views in a port list
entity e is
  port (
    Clock : std_logic;
    Reset : std_logic;
    
    view myInterface1 : bar_b bar;    -- Syntax: view_name space record_name
    view myInterface2 : (foo_b) foo   -- use (view_name) to apply a view to each element of an array  
  );
end entity;

-- Step 7 - Mapping (functions)
map SpamToEgg(view b : foo; sel : integer) to view bar is
begin
-- ...
end map;
```
