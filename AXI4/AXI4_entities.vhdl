use work.axi4_32.all;
entity axil_mem_slave is
    generic (
        ADDR_WIDTH : positive
    );
    port (
        bundle axi : b_axil_slv t_axil(
            wa(addr(ADDR_WIDTH-1 downto 0)),
            ra(addr(ADDR_WIDTH-1 downto 0))
        )
    );
end entity axil_mem_slave

use work.axi4_32.all;
entity axil_cpu is
    port (
        bundle axi : b_axil_mst t_axil(
            wa(addr(31 downto 0)),
            ra(addr(31 downto 0))
        );
        gpio : inout std_logic_vector(15 downto 0)
    );
end entity axil_cpu

use work.axi4_32.all;
entity axil_interconnect is 
    port (
        bundle cpu  : b_axil_slv t_axil;
        bundle mem1 : b_axil_mst t_axil;
        bundle mem2 : b_axil_mst t_axil
    );
end entity axil_interconnect;

use work.axi4_32.all;
entity tb_axil is
end entity tb_axil;

architecture Testbench of tb_axil is

    constant MEM1_AWIDTH : integer := 10;
    constant MEM2_AWIDTH : integer := 22;
    subtype rng_mem1 is integer range MEM1_AWIDTH-1 downto 0:
    subtype rng_mem2 is integer range MEM2_AWIDTH-1 downto 0:

    signal clock_and_reset : t_axi_clock := (
        clk => '0',
        resetn => '1'
    );
    signal bus_cpu  : t_axil(
        wa(addr(31 downto 0)),
        ra(addr(31 downto 0))
    );
    signal bus_mem1 : t_axil( wa(addr(rng_mem1)), ra(addr(rng_mem1)) );
    signal bus_mem2 : t_axil( wa(addr(rng_mem2)), ra(addr(rng_mem2)) );

    signal gpio : std_logic_vector(15 downto 0);

begin

    -- How to tackle the issue of connecting clock to the three busses?
    bus_cpu.clk  <=> clock_and_reset;
    bus_mem1.clk <=> clock_and_reset;
    bus_mem2.clk <=> bus_cpu.clk;
    
    FAKE_RESET : process
    begin
        clock_and_reset.resetn <= '0', '1' after 50 ns;
        wait;
    end process;

    FAKE_CLOCK : process
    begin
        clock_and_reset.clk <= '0', '1' after 5 ns;
        wait for 10 ns;
    end process;
    
    INST_CPU : entity work.axil_cpu
        port map (
            axi => bus_cpu,
            gpio => gpio
        );
        
    INST_INTERCON : entity work.axil_interconnect
        port map (
            cpu => bus_cpu,
            mem1 => bus_mem1,
            mem2 => bus_mem2
        );
        
    INST_MEM1 : entity work.axil_mem_slave
        generic map (
            ADDR_WIDTH => 10
        ) port map (
            axi => mem1
        );
        
    INST_MEM2 : entity work.axil_mem_slave
        generic map (
            ADDR_WIDTH => 22
        ) port map (
            axi => mem2
        );

end architecture Testbench;
