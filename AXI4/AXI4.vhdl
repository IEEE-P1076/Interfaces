-- AXI4-Lite interface using the new proposed views scheme.

package AXI4 is
    -- Address width transformations can be done without logic.  Data width
    -- transformations cannot, and will require an entity to translate between
    -- them.  Therefore, we'll make the data width a package generic, but
    -- leave the address width an unconstrained vector.
    --
    generic (
        DATA_WIDTH : positive := 32
    );
    
    -- AXI uses 5 data ports plus a clock/reset "system interface":
    --  * Write Address     M -> S
    --  * Write Data        M -> S
    --  * Write Response    M <- S
    --  * Read Address      M -> S
    --  * Read Data         M <- S
    --
    -- Each of the 5 "ports" has an independent bidirectional handshake, with
    -- the driver driving (everything else and) valid and the receiver driving
    -- (only) ready.  All ports are synchronous to the same bus clock.

    -- Data width dependent types.
    subtype t_axi_data is std_logic_vector(DATA_WIDTH-1 downto 0);
    subtype t_axi_stb is std_logic_vector((DATA_WIDTH/8)-1 downto 0);

    -- Data width independent types.
    subtype t_axi_prot is std_logic_vector(2 downto 0);
    subtype t_axi_resp is std_logic_vector(1 downto 0);

    type t_axi_clock is record
        clk    : std_logic;
        resetn : std_logic
    end;

    type t_axi_handshake is record
        valid : std_logic;
        ready : std_logic
    end;

    type t_axil_addr is record
        hs   : t_axi_handshake;
        addr : unsigned;
        prot : t_axi_prot
    end;

    type t_axil_writedata is record
        hs   : t_axi_handshake;
        data : t_axi_data;
        strb : t_axi_stb;
    end;

    type t_axil_writeresp is record
        hs   : t_axi_handshake;
        resp : t_axi_resp
    end;

    type t_axil_readdata is record
        hs   : t_axi_handshake;
        data : t_axi_data;
        resp : t_axi_resp
    end;

    type t_axil is record
        wa  : t_axil_addr;
        wd  : t_axil_writedata;
        wb  : t_axil_writeresp;
        ra  : t_axil_addr;
        rd  : t_axil_readdata;
        clk : t_axi_clock
    end;

    -- All lower-level views are defined from the driver's point of view.

    view b_axi_handshake of t_axi_handshake is
        valid : out;
        ready : in;
    end;

    view b_axil_addr of t_axi_addr is
        hs : view v_axi_handshake;
        addr : out;
        prot : out;
    end;

    view b_axil_writedata of t_axil_writedata is
        hs : view v_axi_handshake;
        data : out;
        strb : out;
    end;

    view b_axil_writeresp t_axil_writeresp is
        hs : view v_axi_handshake;
        resp : out;
    end;

    view b_axil_readdata of t_axil_readdata is
        hs   : view v_axi_handshake;
        data : out;
        resp : out;
    end view;

    -- Top level views for master and slave.

    view b_axil_mst of t_axil is
        wa  : view v_axil_addr;
        wd  : view v_axil_writedata;
        wb  : view v_axil_writeresp'reversed;
        ra  : view v_axil_addr;
        rd  : view v_axil_readdata'reversed;
        clk : in t_axi_clock
    end view;

    view b_axil_slv of t_axil is
        wa  : view v_axil_addr'reversed;
        wd  : view v_axil_writedata'reversed;
        wb  : view v_axil_writeresp;
        ra  : view v_axil_addr'reversed;
        rd  : view v_axil_readdata;
        clk : in t_axi_clock
    end view;
end package;

package axi4_32 is new work.axi4 generic map (DATA_WIDTH => 32);
