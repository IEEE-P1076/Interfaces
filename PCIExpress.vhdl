-- Package author:    Patrick Lehmann - Patrick.Lehmann@tu-dresden.de
-- =============================================================================
-- Interface name:    PCI Express (PCIe)
-- Developed by:      PCI Special Interest Group (PCI-SIG)
-- Standard:          
-- Further links:     https://en.wikipedia.org/wiki/Three-state_logic
-- =============================================================================
-- 
use work.I2C.all;
use work.JTAG.all;
use work.LVDS.all;


package PCIe is
	generic (
		LANES : positive
	);

	type B_PCIe_PCB is record
		SMBus    : B_I2C_PCB
		JTAG     : B_JTAG
		PeRst_n  : std_logic;
		Wake_n   : std_logic;
		RefClk   : B_LVDS;
		Lanes    : T_Lane_Vector(LANES - 1 downto 0);
	end record;

	view V_PCIe_PCB of B_PCIe_PCB is
		view SMBus    : view V_I2C_PCB
		view JTAG     : view V_JTAG
		PeRst_n       : in;
		Wake_n        : out;
		view RefClk   : B_LVDS;
		view Lanes    : (B_Lane);
	end view;
	
	type B_PCIe is record
		SMBus    : B_I2C
		JTAG     : B_JTAG
		PeRst_n  : std_logic;
		Wake_n   : std_logic;
		RefClk   : B_LVDS;
		Lanes    : T_Lane_Vector(LANES - 1 downto 0);
	end record;

	view V_PCIe of B_PCIe is
		view SMBus    : view V_I2C
		view JTAG     : view V_JTAG
		PeRst_n       : in;
		Wake_n        : out;
		view RefClk   : B_LVDS;
		view Lanes    : (B_Lane);
	end view;
end package;

-- General Comments:
-- =====================================
-- * <Add your comment here>
