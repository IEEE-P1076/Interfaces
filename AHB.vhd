-------------------------------------------
-- Support Package for AHB-Lite Protocol --
-------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package amba_ahbl_pkg is
    generic (
        DATA_WIDTH : positive := 32
    );

    subtype HADDR_vst   is unsigned(31 downto 0);
    subtype HBURST_vst  is std_logic_vector(3 downto 0);
    subtype HPROT_vst   is std_logic_vector(3 downto 0);
    subtype HSIZE_vst   is std_logic_vector(2 downto 0);
    subtype HTRANS_vst  is std_logic_vector(1 downto 0);

    constant HTRANS_IDLE    : HTRANS_vst := "00";
    constant HTRANS_BUSY    : HTRANS_vst := "01";
    constant HTRANS_NONSEQ  : HTRANS_vst := "10";
    constant HTRANS_SEQ     : HTRANS_vst := "11";
    
    constant HBURST_SINGLE  : HBURST_vst := "000";  -- Single burst
    constant HBURST_INCR    : HBURST_vst := "001";  -- Incrementing burst of undefined length
    constant HBURST_WRAP4   : HBURST_vst := "010";  -- 4-beat wrapping burst
    constant HBURST_INCR4   : HBURST_vst := "011";  -- 4-beat incrementing burst
    constant HBURST_WRAP8   : HBURST_vst := "100";  -- 8-beat wrapping burst
    constant HBURST_INCR8   : HBURST_vst := "101";  -- 8-beat incrementing burst
    constant HBURST_WRAP16  : HBURST_vst := "110";  -- 16-beat wrapping burst
    constant HBURST_INCR16  : HBURST_vst := "111";  -- 16-beat incrementing burst

    -----------------------------------------------------
    -- Record & Array Types for AMBA AHB-Lite Protocol --
    -----------------------------------------------------
    
    subtype ahbl_data is std_logic_vector(DATA_WIDTH-1 downto 0);
    
    type t_ahbl_globals is record
        HCLK    : std_logic;
        HRESET  : std_logic;
    end record t_ahbl_globals;

    type t_ahbl_mosi is record
        HADDR       : HADDR_vst;            -- AHBL address bus (32b)
        HBURST      : HBURST_vst;           -- AHBL burst type
        HMASTLOCK   : std_logic;            -- AHBL locked transfer control
        HPROT       : HPROT_vst;            -- AHBL access protection control
        HSIZE       : HSIZE_vst;            -- AHBL transfer size control
        HTRANS      : HTRANS_vst;           -- AHBL transfer type control
        HWDATA      : ahbl_data;            -- AHBL write data bus (min 32 bits)
        HWRITE      : std_logic             -- AHBL transfer write/read control
    end record t_ahbl_mosi;
    
    type t_ahbl_miso is record
        HRDATA      : ahbl_data;            -- AHBL read data bus (min 32 bits)
        HREADY      : std_logic;            -- AHBL transfer complete
        HRESP       : std_logic             -- AHBL transfer response
    end record t_ahbl_miso;
    
    type t_ahbl_master is record
        mosi : t_ahbl_mosi;
        miso : t_ahbl_miso;
        gbl  : t_ahbl_globals
    end record t_ahbl_master;
    
    type t_ahbl_slave is record
        mosi : t_ahbl_mosi;
        miso : t_ahbl_miso;
        HSEL : std_logic;
        gbl  : in  ahbl_globals
    end record t_ahbl_slave;
    
    type ta_ahbl_miso is array(natural range <>) of t_ahbl_miso;
    
    -----------------------------------------------------------------------
    --  views for directionality
    -----------------------------------------------------------------------
    
    view b_ahbl_master of t_ahbl_master is
        mosi : out;
        miso : in;
        gbl  : in
    end view b_ahbl_master;
    
    view b_ahbl_slave of t_ahbl_slave is
        mosi : in;
        miso : out;
        hsel : in;
        gbl  : in
    end view b_ahbl_slave;
    
    -- TODO: I have _no idea_ what sort of syntax we're looking at
    -- inside of the map functions; below is almost certainly wrong.
    --
    map attach_slave(
        view mst  : view v_ahbl_master t_ahbl_master;
        view slvs : (b_ahbl_slave) ta_ahbl_miso;
        hsel : in std_logic_vector;
        constant n  : in natural
    ) to view b_ahbl_slave is
    begin
        return (
            mosi => mst.mosi,
            miso => slvs(n).miso,
            hsel => hsel(n),
            gbl  => mst.gbl
        );
    end attach_slave;
            
end package amba_ahbl_pkg;

package body amba_ahbl_pkg;
end package body amba_ahbl_pkg;

package amba_ahbl_pkg32 is new amba_ahbl_pkg(DATA_WIDTH => 32);

--------------------------------
-- Context Clause for Package --
--------------------------------
context amba_ahbl32_context;
    library ieee;
    use ieee.std_logic.1164.all;
    use work.amba_ahbl_pkg32.all;
end context amba_ahbl32_context;

-----------------------------------------------------------------------
--  Declare entities for the master, slave and interconnect
-----------------------------------------------------------------------

-- Wrap the decoder and multiplexer into a single interconnect block.
context amba_ahbl32_context;
entity ahbl_interconnect is
    generic (
        ADDR_START : integer_vector;
        ADDR_END   : integer_vector
    );
    port (
        HADDR   : in HADDR_vst;
        HSEL    : out std_logic_vector;
        
        slv     : in ta_ahbl_miso;
        mst     : out ahbl_slave
    );
end entity ahbl_interconnect;

context amba_ahbl32_context;
entity ahbl_cpu_vonneumann is
    port (
        view iface : view v_ahbl_master t_ahbl_master
    );
end entity ahbl_cpu_haahvahd;

context amba_ahbl32_context;
entity ahbl_ram is
    generic (
        ADDR_BITS : positive
    );
    port (
        view iface : view v_ahbl_slave t_ahbl_slave;
    );
end entity ahbl_ram;

-----------------------------------------------------------------------
--  Top Level
-----------------------------------------------------------------------

context amba_ahbl32_context;

entity top_level is
    port(
        clk : in std_logic;
        rst : in std_logic
    );
end entity top_level;

architecture Structural of top_level is

   HSEL          : std_logic_vector(1 downto 0);
   ambabus       : t_ahbl_master;
   slvs          : ta_ahbl_miso;
   
   constant INST_RAM_WIDTH : integer := 10;
   constant DATA_RAM_WIDTH : integer := 10;
   
begin(
    
    -- TODO: We need to get the ports onto the internal records.  We still
    -- have no syntax for this, so the spaceship it is.  These two examples
    -- should be equivalent.

    ambabus.gbl <=> (
        HCLK => clk,
        HRESET <=> rst
    );
    -- ambabus.gbl.HCLK <=> clk;
    -- ambabus.gbl.HRESET <=> rst;

    -- Master Instantiation for AHB-Lite Block
    CPU : entity work.ahbl_cpu_vonneumann is
    port (
        iface => ambabus
    );

    -- Slave interconnect, decoder and multiplexer
    INTERCON : entity work.ahbl_interconnect is
        generic map (
            ADDR_START => (0, 2**16),
            ADDR_END   => (2**INST_RAM_WIDTH, 2**16+2**DATA_RAM_WIDTH)
        ) port map (
            HADDR   => ambabus.mosi.HADDR,
            HSEL    => HSEL,
            
            slv     => slvs,
            mst     => ambabus.miso
        );
    
    -- RAM slaves.
    
    IRAM: entity work.ahbl_ram is
        generic map (
            ADDR_BITS => INST_RAM_WIDTH
        ) port map (
            iface => attach_slave(mst, slvs, HSEL, 0)
        );

    DRAM: entity work.ahbl_ram is
        generic map (
            ADDR_BITS => DATA_RAM_WIDTH
        ) port map (
            iface => attach_slave(mst, slvs, HSEL, 1)
        );
        
end architecture Structural;
