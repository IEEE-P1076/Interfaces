-- Package author:    Patrick Lehmann - Patrick.Lehmann@tu-dresden.de
-- =============================================================================
-- Interface name:    VGA
-- Developed by:      IBM
-- Standard/Manual:   http://www.mcamafia.de/pdf/ibm_vgaxga_trm2.pdf
-- Further links:     https://en.wikipedia.org/wiki/VGA_connector
-- =============================================================================
-- 
use work.I2C.all;


package VGA is
  type B_VGA is record
    HSync : std_logic;
    VSync : std_logic;
    R     : std_logic_vector;
    G     : std_logic_vector;
    B     : std_logic_vector;
  end record;
	
  view V_VGA is
    HSync : out;
    VSync : out;
    R     : out;
    G     : out;
    B     : out;
  end view;

  -- DDC - Display Data Channel
  type B_VGA_with_DCC_PCB is record
    VGA : B_VGA;
    DDC : B_I2C_PCB;
  end record;
  
  view V_VGA_PCB of B_VGA_with_DCC_PCB is
    VGA : view V_VGA;
    DDC : view V_I2C_PCB;
  end view;

  -- DDC - Display Data Channel
  type B_VGA_with_DCC is record
    VGA : B_VGA;
    DDC : B_I2C;
  end record;
  
  view V_VGA of B_VGA_with_DCC is
    VGA : view V_VGA;
    DDC : view V_I2C;
  end view;
end package;

-- General Comments:
-- =====================================
-- * <Add your comment here>
